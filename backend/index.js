
import WebSocket from 'ws'
import express from 'express'
import nact from 'nact'

import createRoot from './actors/root.js'
import createPlayerConn from './actors/playerConn.js'

// TODO ping/pong message support

// TODO root actor

// TODO lobby message types

const httpServer = express()
  .use(express.static('../frontend/public')) // TODO is this where the built output will be?
  .listen(8080, () => console.log('HTTP server running'))

const wsServer = new WebSocket.Server({ server: httpServer })

const { actorSystem, rootActor } = createRoot()

wsServer.on('connection', (ws, req) => {
  const remoteAddress = req.socket.remoteAddress

  console.log(`Connection received from ${remoteAddress}`)

  const playerConn = createPlayerConn(rootActor, ws)
})

