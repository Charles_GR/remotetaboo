
import nact from 'nact'

import gameStates from '../gameStates.js'

import * as playerConn from './playerConn.js'
import createLobby from './lobby.js'
import createGame from './game.js'

export default function createSystem() {

  const system = nact.start()
  const root = nact.spawn(system, onMessage({ gameState: gameStates.UNINITIALISED }), 'tabooRoot')

  nact.dispatch(root, { type: MSG_TYPES.INITIALISE }, root)

  return { actorSystem: system, rootActor: root }
}

export const MSG_TYPES = {
  INITIALISE: 'INITIALISE',
  START_GAME: 'START_GAME'
}

export const TO_PLAYER_MSG_TYPES = {
  ERROR: 'ERROR'
}

const onMessage = (initialState) => (state = initialState, msg, ctx) => {
  switch (msg.type) {

    case MSG_TYPES.INITIALISE:
      console.log('Initialising game')
      const lobby = createLobby(ctx.sender)
      return {
        gameState: 'LOBBY',
        stateActor: lobby
      }

    case playerConn.MSG_TYPES.FROM_PLAYER:
      console.log(`Relaying a ${msg.subType} from ${msg.playerId}`)
      nact.dispatch(state.stateActor, msg, ctx.sender)
      return state

    case MSG_TYPES.START_GAME:
      if (state.gameState === gameStates.LOBBY) {
        console.log(`starting the game from ${msg.playerId}`)
        const game = createGame(ctx.sender, msg.players)
        return {
          gameState: 'STARTED',
          stateActor: game
        }
      }
      break
  }
}
