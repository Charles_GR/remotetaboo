
import nact from 'nact'
import { v4 as uuidV4 } from 'uuid'

import { MSG_TYPES as ROOT_MSG_TYPES} from './root.js'
import * as lobby from './lobby.js'

export const MSG_TYPES = {
  FROM_PLAYER: 'FROM_PLAYER',
  TO_PLAYER: 'TO_PLAYER'
}

const TO_PLAYER_MSG_TYPES = {
  ACCEPTED: 'ACCEPTED'
}

export default function createPlayerConn(root, ws) {
  const playerId = uuidV4()

  const playerConn = nact.spawn(root, onMessage({ playerId, ws }), `player-${playerId}`)

  ws.on('message', (wsMsgStr) => {
    console.log(`Received ${wsMsgStr} from ${playerId}`)

    let wsMsg
    try {
      wsMsg = JSON.parse(wsMsgStr)
    } catch (e) {
      console.error(`Could not decode JSON of WS msg for ${playerId}:`, e)
      return
    }

    nact.dispatch(root, {
      type: MSG_TYPES.FROM_PLAYER,
      subType: wsMsg.type,
      playerId,
      payload: wsMsg.payload
    })
  })

  send(ws, {
    type: TO_PLAYER_MSG_TYPES.ACCEPTED,
    payload: {
      playerId
    }
  })

  nact.dispatch(root, {
    type: MSG_TYPES.FROM_PLAYER,
    subType: lobby.FROM_PLAYER_MSG_TYPES.PLAYER_JOINED,
    playerId,
    payload: {
      connection: playerConn
    }
  })

  return playerConn
}

const onMessage = (initialState) => (state = initialState, msg, ctx) => {
  switch (msg.type) {
    case MSG_TYPES.TO_PLAYER:
      if (msg.playerId === state.playerId) {
        send(state.ws, {
          type: msg.subType,
          payload: msg.payload
        })

        console.log(`Forwarded a ${msg.subType} to ${msg.playerId}`)
      } else {
        console.error(`Connection for ${state.playerId} got ${MSG_TYPES.TO_PLAYER} for wrong player ${msg.playerId}`)
      }

      return state
  }
}

function send(ws, msg) {
  ws.send(JSON.stringify(msg))
}

export function broadcastToPlayers(players, playerMsg, sender) {
  for (let player of players) {
    nact.dispatch(player.connection, {
      type: MSG_TYPES.TO_PLAYER,
      subType: playerMsg.type,
      playerId: player.id,
      payload: playerMsg.payload
    }, sender)
  }
}
