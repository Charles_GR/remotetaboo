import nact from 'nact'

import * as root from './root.js'
import * as playerConn from './playerConn.js'

export default function createLobby(root) {
  return nact.spawn(root, onMessage, 'lobby')
}

export const FROM_PLAYER_MSG_TYPES = {
  PLAYER_JOINED: 'PLAYER_JOINED',
  SET_NAME: 'SET_NAME',
  START_GAME: 'START_GAME'
}

const TO_PLAYER_MSG_TYPES = {
  LOBBY_STATE: 'LOBBY_STATE'
}

function onMessage(state = {}, msg, ctx) {
  switch (msg.type) {

    case playerConn.MSG_TYPES.FROM_PLAYER:
      const playerMsg = {
        ...msg.payload,
        type: msg.subType,
        playerId: msg.playerId
      }

      return onPlayerMessage(state, playerMsg, ctx)
  }
}

function onPlayerMessage(state, msg, ctx) {
  console.log(msg)
  switch (msg.type) {
    case FROM_PLAYER_MSG_TYPES.PLAYER_JOINED:
      state[msg.playerId] = {
        id: msg.playerId,
        name: 'Unnamed player',
        connection: msg.connection
      }

      console.log(`lobby acknowledged new player ${msg.playerId}`)

      broadcastState(state, ctx.self)

      return state

    case FROM_PLAYER_MSG_TYPES.SET_NAME:
      const player = state[msg.playerId]
      const oldName = player.name
      player.name = msg.newName

      console.log(`Player ${msg.playerId} renamed from "${oldName} to ${msg.newName}`)

      broadcastState(state, ctx.self)

      return state

    case FROM_PLAYER_MSG_TYPES.START_GAME:
      if (Object.keys(state).length < 4) {
        console.error(`${msg.playerId} tried to start a game with less than 4 players`)

        nact.dispatch(state[msg.playerId].connection, {
          type: playerConn.MSG_TYPES.TO_PLAYER,
          subType: root.TO_PLAYER_MSG_TYPES.ERROR,
          playerId: msg.playerId,
          payload: 'At least 4 players are required to start the game'
        }, ctx.self)
      } else {
        nact.dispatch(ctx.parent, {
          type: root.MSG_TYPES.START_GAME,
          players: state
        }, ctx.self)
      }

      return state
  }
}

function broadcastState(state, sender) {
  const lobbyState = Object.fromEntries(Object.entries(state).map(([id, player]) => [id, player.name]))

  console.log(`Broadcasting new lobby state: ${JSON.stringify(lobbyState)}`)

  playerConn.broadcastToPlayers(Object.values(state), {
    type: TO_PLAYER_MSG_TYPES.LOBBY_STATE,
    payload: lobbyState
  }, sender)
}
