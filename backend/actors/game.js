
import nact from 'nact'
import _ from 'lodash'
import words from "../words.js";

import * as root from './root.js'
import * as playerConn from './playerConn.js'

const RED = 'RED'
const BLUE = 'BLUE'

const FROM_PLAYER_MSG_TYPES = {
  WORD_GUESSED: 'WORD_GUESSED',
  SQUEAK: 'SQUEAK'
}

const TO_PLAYER_MSG_TYPES = {
  TURN_CHANGE: 'TURN_CHANGE',
  GAME_OVER: 'GAME_OVER'
}

const TURN_CHANGE_REASONS = {
  INITIAL: 'INITIAL',
  GUESSED: 'GUESSED',
  SQUEAKED: 'SQUEAKED'
}

export default function createGame(root, players) {
  const shuffledPlayers = _.shuffle(Object.values(players))

  const [redPlayers, bluePlayers] = _.chunk(shuffledPlayers, Math.ceil(shuffledPlayers.length/2))

  function initTeamState(teamPlayers) {
    return {
      score: 0,
      players: teamPlayers,
      describerOrder: [...teamPlayers],
      refereeOrder: _.shuffle(teamPlayers)
    }
  }

  const initialState = {
    players,
    redTeam: initTeamState(redPlayers),
    blueTeam: initTeamState(bluePlayers),
    currentGuessingTeam: _.sample([RED, BLUE]),
    // TODO rethink how many turns the game should last
    remainingWords: _.sampleSize(words, shuffledPlayers.length)
    // Other fields filled in by the change to the first turn
  }

  const game = nact.spawn(root, onMessage(initialState), 'game')

  nextTurn(TURN_CHANGE_REASONS.INITIAL, initialState, game)

  return game
}


const onMessage = (initialState) => (state = initialState, msg, ctx) => {
  switch (msg.type) {

    case playerConn.MSG_TYPES.FROM_PLAYER:
      const playerMsg = {
        ...msg.payload,
        type: msg.subType,
        playerId: msg.playerId
      }

      return onPlayerMessage(state, playerMsg, ctx)
  }
}

function onPlayerMessage(state, msg, ctx) {
  switch (msg.type) {

    case FROM_PLAYER_MSG_TYPES.WORD_GUESSED:
      if (!isPlayerAGuesser(msg.playerId, state)) {
        console.error(`Got ${FROM_PLAYER_MSG_TYPES.WORD_GUESSED} from player on wrong team ${msg.playerId}. ` +
          `Was expecting ${state.currentGuessingTeam}`)

        return state
      }

      if (state.currentGuessingTeam === RED) {
        state.redTeam.score++
      } else {
        state.blueTeam.score++
      }
      nextTurn(TURN_CHANGE_REASONS.GUESSED, state, ctx.self)

      return state

    case FROM_PLAYER_MSG_TYPES.SQUEAK:
      if (msg.playerId !== state.referee.id) {
        console.error(`Got ${FROM_PLAYER_MSG_TYPES.SQUEAK} from wrong player ${msg.playerId}. ` +
          `Was expecting ${state.referee.id}`)

        return state
      }

      nextTurn(TURN_CHANGE_REASONS.SQUEAKED, state, ctx.self)

      return state
  }
}


function nextTurn(reason, state, self) {
  if (state.remainingWords.length > 0) {
    const nextWord = state.remainingWords.shift()

    const [nextGuessingTeamName, nextRefereeingTeamName] = state.currentGuessingTeam === RED ? [BLUE, RED] : [RED, BLUE]
    const nextGuessingPlayers = (nextGuessingTeamName === RED ? state.redTeam : state.blueTeam)
    const nextRefereeingPlayers = (nextRefereeingTeamName === RED ? state.redTeam : state.blueTeam)

    const describer = nextGuessingPlayers.describerOrder.shift()
    nextGuessingPlayers.describerOrder.push(describer)

    const referee = nextRefereeingPlayers.refereeOrder.shift()
    nextRefereeingPlayers.refereeOrder.push(referee)

    state.currentGuessingTeam = nextGuessingTeamName
    state.currentWord = nextWord
    state.describer = describer
    state.referee = referee

    broadcastTurnChange(reason, state, self)

  } else {
    endGame(state, self)
  }
}

function broadcastTurnChange(turnChangeReason, state, self) {
  const payload = {
    redTeam: toExternalTeam(state.redTeam),
    blueTeam: toExternalTeam(state.blueTeam),
    currentGuessingTeam: state.currentGuessingTeam,
    currentWord: state.currentWord,
    describer: toExternalPlayer(state.describer),
    referee: toExternalPlayer(state.referee),
    turnChangeReason
  }

  playerConn.broadcastToPlayers(Object.values(state.players), {
    type: TO_PLAYER_MSG_TYPES.TURN_CHANGE,
    payload
  }, self)
}

function endGame(state, self) {
  let winner
  if (state.redTeam.score === state.blueTeam.score) {
    winner = 'DRAW'
  } else if (state.redTeam.score > state.blueTeam.score) {
    winner = RED
  } else {
    winner = BLUE
  }

  const payload = {
    winner,
    redTeam: toExternalTeam(state.redTeam),
    blueTeam: toExternalTeam(state.blueTeam)
  }

  playerConn.broadcastToPlayers(Object.values(state.players), {
    type: TO_PLAYER_MSG_TYPES.GAME_OVER,
    payload
  }, self)
}

function toExternalTeam(internalTeam) {
  return {
    score: internalTeam.score,
    players: internalTeam.players.map(toExternalPlayer)
  }
}

function toExternalPlayer(internalPlayer) {
  return {
    id: internalPlayer.id,
    name: internalPlayer.name
  }
}

function isPlayerAGuesser(id, state) {
  const currentGuessingPlayers = (state.currentGuessingTeam === RED ? state.redTeam.players : state.blueTeam.players)

  return currentGuessingPlayers.some((player) => player.id === id)
}
