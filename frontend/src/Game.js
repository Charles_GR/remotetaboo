import React, { useState } from 'react';
import RoleView from './RoleView';

export default function Game(props) {
  const [ role, setRole ] = useState('guesser');
  const [ team, setTeam ] = useState(null);
  const [ words, setWords ] = useState({ main: '', forbidden: [] });
  const [ started, setStarted ] = useState(false);
  const [ gameOver, setGameOver ] = useState(false);
  const [ winningTeam, setWinningTeam ] = useState(null);
  const [ redTeamScore, setRedTeamScore ] = useState(null);
  const [ blueTeamScore, setBlueTeamScore ] = useState(null);

  if(props.gameState && !started) {
    setStarted(true);
    const payload = props.gameState.payload;

    if(payload.redTeam.players.some(player => player.name === props.name))
    {
      setTeam('red');
    }
    else if(payload.blueTeam.players.some(player => player.name === props.name))
    {
      setTeam('blue');
    }

    setWords({main: payload.currentWord[0], forbidden: payload.currentWord[1] });

    if(payload.describer.name === props.name) {
      setRole('describer');
    }
    else if(payload.referee.name === props.name) {
      setRole('referee');
    }
    else {
      setRole('guesser');
    }
  }

  props.webSocket.onmessage = function(event) {
    const data = JSON.parse(event.data);
    const payload = data.payload;
    if (data.type === 'TURN_CHANGE') {
      if (payload.turnChangeReason === 'SQUEAKED') {
        new Audio('http://www.suonoelettronico.com/waves22/POLWHST2.mp3').play();
      }
      setWords({main: payload.currentWord[0], forbidden: payload.currentWord[1]});
      if (payload.describer.name === props.name) {
        setRole('describer');
      } else if (payload.referee.name === props.name) {
        setRole('referee');
      } else {
        setRole('guesser');
      }
    } else if (data.type === 'GAME_OVER') {
      setWinningTeam(payload.winner);
      setGameOver(true);
      setRedTeamScore(payload.redTeam.score);
      setBlueTeamScore(payload.blueTeam.score);
    }
  };

  function getResult() {
    if(winningTeam === 'DRAW')
    {
      return "YOU DREW!";
    }
    else if(team.toLowerCase() === winningTeam.toLowerCase())
    {
      return "YOU WON!";
    }
    else
    {
      return "YOU LOST!";
    }
  }

  if(gameOver) {
    return (
      <>
        <div>Your Team: {team}</div>
        <div>Red Team Score: {redTeamScore}</div>
        <div>Blue Team Score: {blueTeamScore}</div>
        <div>Winning Team: {winningTeam}</div>
        <div>{getResult()}</div>
      </>
    )
  }
  else {
    return (
      <>
        <div>
          <div>Team: {team}</div>
          <div>Role: {role}</div>
        </div>
        <div style={{ marginTop: '10px' }}>
          <RoleView
            role={role}
            words={words}
            webSocket={props.webSocket}
            team={team}
          />
        </div>
      </>
    );
  }
}