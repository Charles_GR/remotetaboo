import React from 'react';
import { useState } from 'react';
import NameInput from './NameInput';
import Game from './Game';
import Lobby from './Lobby';

export default function PageView(props) {
  const [ gameState, setGameState ] = useState(null);

  switch(props.view) {
    case 'nameInput':
      return (
        <NameInput
          setView={props.setView}
          webSocket={props.webSocket}
          setWebSocket={props.setWebSocket}
          connected={props.connected}
          name={props.name}
          setName={props.setName}
        />
      );
    case 'lobby':
      return (
        <Lobby
          setView={props.setView}
          webSocket={props.webSocket}
          gameState={gameState}
          setGameState={setGameState}
          name={props.name}
        />
      );
    case 'game':
      return (
        <Game
          setView={props.setView}
          webSocket={props.webSocket}
          gameState={gameState}
          name={props.name}
        />
      );
    default:
  }
}