import React from 'react';
import GameCard from './GameCard';

export default function RefereeView(props) {
  function blowWhistle() {
    props.webSocket.send(JSON.stringify({"type": "SQUEAK"}));
  }

  return (
    <>
      <span>Listen for speaker saying any of the disallowed words and if so then blow the whistle using the icon.</span>
      <div style={{ marginTop: '10px'}}>
        <GameCard mainWord={props.words.main} forbiddenWords={props.words.forbidden}/>
      </div>
      <div style={{ marginTop: '10px' }}>
        <input
          type="image"
          width="100px"
          height="70px"
          alt="Whistle"
          src="https://www.pngfind.com/pngs/m/376-3769578_27-oct-coaching-icon-01-whistle-png-transparent.png"
          onClick={blowWhistle}
        />
      </div>
    </>
  );
}