import React from 'react';
import GuesserView from './GuesserView';
import RefereeView from './RefereeView';
import DescriberView from './DescriberView';

export default function RoleView(props) {
  switch(props.role) {
    case 'describer':
      return (
        <DescriberView
          words={props.words}
        />
      );
    case 'referee':
      return (
        <RefereeView
          words={props.words}
          webSocket={props.webSocket}
        />
      );
    case 'guesser':
      return (
        <GuesserView
          words={props.words}
          webSocket={props.webSocket}
        />
      );
    default:
  }
}