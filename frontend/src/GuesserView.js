import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';

export default function GuesserView(props) {
  const [ guess, setGuess ] = useState('');
  const [ correct, setCorrect ] = useState(false);
  const [ wrong, setWrong ] = useState(false);

  async function submitGuess() {
    if(guess.toLowerCase() === props.words.main.toLowerCase()) {
      setWrong(false);

      setCorrect(true);
      await new Promise(resolve => setTimeout(resolve, 1000));
      setCorrect(false);

      setGuess('');
      props.webSocket.send(JSON.stringify({"type": "WORD_GUESSED"}));
    }
    else {
      setWrong(true);
    }
  }

  return (
    <>
      <div>
        Guess the word that is being described by the speaker and enter it below.
      </div>
      <div style={{ marginTop: '10px' }}>
        <input
          type="text"
          value={guess}
          onChange={event => setGuess(event.target.value)}
        />
        <Button style={{ marginLeft: '5px' }} onClick={submitGuess}>
          Submit Guess
        </Button>
      </div>
      <div style={{ marginTop: '10px' }}>
        <img
          src="https://www.pinclipart.com/picdir/middle/127-1271755_correct-clipart-clipground-approve-icon-png-transparent-png.png"
          width="100px"
          height="100px"
          hidden={!correct}
        />
        <img
          src="https://www.clipartmax.com/png/middle/100-1005122_cancelled-close-delete-exit-no-reject-wrong-icon-red-cross-no-entry.png"
          width="100px"
          height="100px"
          hidden={!wrong}
        />
      </div>
    </>
  );
}