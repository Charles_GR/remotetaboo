import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import ListGroup from 'react-bootstrap/ListGroup';

export default function Lobby(props) {
  const [ players, setPlayers ] = useState([]);

  useEffect(() => {
    console.log('effect');

    if(props.gameState) {
      props.setView('game');
    }
  }, [ props.gameState ]);

  props.webSocket.onmessage = function(event) {
    const data = JSON.parse(event.data);
    if(data.type === 'LOBBY_STATE') {
      setPlayers(Object.values(data.payload));
    }
    else if(data.type === 'TURN_CHANGE') {
      props.setGameState(data);
    }
  };

  function startGame() {
    props.webSocket.send(JSON.stringify({"type": "START_GAME"}));
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <div style={{ width: '200px', display: 'inline-block'}}>
        <div>
          <label>Players:</label>
        </div>
        <div>
          <ListGroup>
            {
              players.map((player, index) => (
              <ListGroup.Item key={index} variant="primary">
                {player}
              </ListGroup.Item>
              ))
            }
          </ListGroup>
        </div>
      </div>
      <div style={{ marginTop: '20px' }}>
        <Button variant="primary" onClick={startGame}>
          Start Game
        </Button>
      </div>
    </div>
  );
}