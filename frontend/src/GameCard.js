import React from 'react';
import { Card, ListGroup } from 'react-bootstrap';

export default function GameCard(props) {
  return (
    <div style={{ textAlign: 'center' }}>
      <div style={{ display: 'inline-block' }}>
        <Card style={{ width: '18rem' }} bg="primary">
          <Card.Header>{props.mainWord}</Card.Header>
          <ListGroup>
            {
              props.forbiddenWords.map((word, index) => (
                <ListGroup.Item key={index}>
                  {word}
                </ListGroup.Item>
              ))
            }
          </ListGroup>
        </Card>
      </div>
    </div>
  );
}