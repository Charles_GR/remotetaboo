import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import Spinner from 'react-bootstrap/Spinner';

export default function NameInput(props) {
  const [ loading, setLoading ] = useState(false);

  useEffect(() => {
    if(props.webSocket) {
      props.webSocket.onopen = function () {
        props.webSocket.onmessage = function(event) {
          setLoading(false);
          const data = JSON.parse(event.data);
          if(data.type === 'ACCEPTED') {
            props.setView('lobby');
          }
        };
        props.webSocket.send(JSON.stringify({"type": "SET_NAME", "payload": {"newName": `${props.name}`}}));
      };
    }
  }, [ props.webSocket ]);

  function enterLobby () {
    setLoading(true);
    props.setWebSocket(new WebSocket('ws://localhost:8080'));
  }

  return (
    <div style={{ textAlign: 'center' }}>
      <div style={{ width: '200px', display: 'inline-block' }}>
        <FormControl
          placeholder="Name"
          value={props.name}
          onChange={event => {props.setName(event.target.value)}}
          maxLength={15}
        />
      </div>
      <div style={{ marginTop: '20px' }}>
        <Button onClick={enterLobby} disabled={loading || !props.name}>
          Enter Lobby
        </Button>
      </div>
      <div style={{ marginTop: '20px' }} hidden={!loading}>
        <Spinner animation="border" variant="primary" />
      </div>
    </div>
  );
}