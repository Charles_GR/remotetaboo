import React from 'react';
import GameCard from './GameCard';

export default function DescriberView(props) {
  return (
    <>
      <span>Describe the word without saying any of the disallowed words below it.</span>
      <div style={{ marginTop: '10px'}}>
        <GameCard mainWord={props.words.main} forbiddenWords={props.words.forbidden}/>
      </div>
    </>
  );
}