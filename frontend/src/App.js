import React, { useState } from 'react';
import PageView from './PageView';

import './App.css';

export default function App() {
  const [ webSocket, setWebSocket ] = useState(null);
  const [ view, setView ] = useState('nameInput');
  const [ name, setName ] = useState('');

  return (
    <>
      <div style = {{ textAlign: 'center', marginBottom: '50px' }}>
        <h1>Taboo</h1>
      </div>
      <div style={{ textAlign: 'center' }}>
        <PageView
          view={view}
          setView={setView}
          webSocket={webSocket}
          setWebSocket={setWebSocket}
          name={name}
          setName={setName}
        />
      </div>
    </>
  );
}